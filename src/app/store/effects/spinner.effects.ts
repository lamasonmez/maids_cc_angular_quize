import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NgxSpinnerService } from 'ngx-spinner';
import { tap } from 'rxjs/operators';

import * as fromUserActiosn from '../../modules/users/state/user.actions';

@Injectable()
export class SpinnerEffects {
	spinneron$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(fromUserActiosn.loadUsers, fromUserActiosn.searchUsers),
				tap(() => this.spinner.show())
			),
		{ dispatch: false }
	);
	spinneroff$ = createEffect(
		() =>
			this.actions$.pipe(
				ofType(
					fromUserActiosn.loadUsersSuccess,
					fromUserActiosn.loadUsersFailure,
					fromUserActiosn.searchUsersSuccess,
					fromUserActiosn.searchUsersFailure
				),
				tap(() => {
					setTimeout(() => {
						this.spinner.hide();
					}, 300);
				})
			),
		{ dispatch: false }
	);

	constructor(private actions$: Actions, private spinner: NgxSpinnerService) {}
}
