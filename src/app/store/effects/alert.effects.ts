import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AlertService } from 'ngx-alerts';
import { tap } from 'rxjs/operators';
import * as fromUserActiosn from '../../modules/users/state/user.actions';



@Injectable()
export class AlertEffects {

  unableToLoadUsers$=createEffect(
    ()=>
    this.actions$.pipe(
      ofType(fromUserActiosn.loadUsersFailure),
      tap(()=>
      setTimeout(()=>{
        this.alertService.danger('unable to load users')
      },2000)
      )
    ),
    {dispatch: false}
  );
  UserNotFound$=createEffect(
    ()=>
    this.actions$.pipe(
      ofType(fromUserActiosn.searchUsersFailure),
      tap(()=>
      setTimeout(()=>{
        this.alertService.danger('User Not FOund')
      },2000)
      )
    ),
    {dispatch: false}
  );
  constructor(private actions$: Actions,private alertService: AlertService) {}

}
