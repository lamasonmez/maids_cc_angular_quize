import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import * as fromUserActions from './state/user.actions';
import { User } from './state/user.model';
import * as UserSelector from './state/user.selectors';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: [ './users.component.scss' ]
})
export class UsersComponent implements OnInit {
	vm$: Observable<UserSelector.UersViewModel> | null = null;
	searchResult$: Observable<User>;

	constructor(private store: Store<AppState>) {}
	ngOnInit(): void {
		this.store.dispatch(fromUserActions.loadUsers({ page: '1' }));

		this.vm$ = this.store.pipe(select(UserSelector.selectUsersViewModel));

		this.searchResult$ = this.store.pipe(select(UserSelector.selectUser));
	}
	onChangePage(page: string) {
		// update current page of items
		this.store.dispatch(fromUserActions.loadUsers({ page: page }));
	}
	getAll() {
		this.searchResult$ = null;
	}
}
