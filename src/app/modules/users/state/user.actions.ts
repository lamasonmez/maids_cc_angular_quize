import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { User } from './user.model';
import { PaginatedResult } from 'src/app/shared/models/pagination';

/************************************************/
/*** LOAD Users Actions***/
/************************************************/
export const loadUsers = createAction(
  '[User/API Users Component] Load Users',
  props<{page:string}>()
);

export const loadUsersSuccess = createAction(
  '[Users Effect] Load Users Success',
  props<{paginatedResults:PaginatedResult<User[]>}>()
);



export const loadUsersFailure = createAction(
  '[Users Effect] Load Users Failure',
  props<{error:any}>()
);

export const searchUsers = createAction(
  '[User/API Users Component] Search Users',
  props<{search:number}>()
);

export const searchUsersSuccess = createAction(
  '[Users Effect] Search Users Success',
  props<{paginatedResults:PaginatedResult<User[]>}>()
);
export const searchUsersFailure = createAction(
  '[Users Effect] Search Users Failure',
  props<{error:any}>()
);

export const addUser = createAction(
  '[User/API] Add User',
  props<{ user: User }>()
);

export const upsertUser = createAction(
  '[User/API] Upsert User',
  props<{ user: User }>()
);

export const addUsers = createAction(
  '[User/API] Add Users',
  props<{ users: User[] }>()
);

export const upsertUsers = createAction(
  '[User/API] Upsert Users',
  props<{ users: User[] }>()
);

export const updateUser = createAction(
  '[User/API] Update User',
  props<{ user: Update<User> }>()
);

export const updateUsers = createAction(
  '[User/API] Update Users',
  props<{ users: Update<User>[] }>()
);

export const deleteUser = createAction(
  '[User/API] Delete User',
  props<{ id: string }>()
);

export const deleteUsers = createAction(
  '[User/API] Delete Users',
  props<{ ids: string[] }>()
);

export const clearUsers = createAction(
  '[User/API] Clear Users'
);
