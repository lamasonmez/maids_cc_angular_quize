import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as UserReducer from './user.reducer';
import { Pagination } from 'src/app/shared/models/pagination';
import { User } from './user.model';
import { AppState } from 'src/app/store';

export const selectUsersState = createFeatureSelector<UserReducer.State>(UserReducer.usersFeatureKey);

export const selectAllUsers = createSelector(selectUsersState, UserReducer.selectAll);

export const selectPagination = createSelector(selectUsersState, (state: UserReducer.State) => state.pagination);
export const selectId = createSelector(selectUsersState, (state: UserReducer.State) => state.selectedUserId);
export const selectUser = createSelector(selectUsersState, (state: UserReducer.State) => state.user);

/********************************************************************************* */
/****RETURN Users VIEW MODEL */
/********************************************************************************* */

export interface UersViewModel {
	pagination: Pagination;
	users: User[];
}

export const selectUsersViewModel = createSelector(
	selectPagination,
  selectAllUsers,
	(pagination, users,props:?any): UersViewModel => {
    if(props && props.search!=null){
      console.log('props * search',props , props.search)
      return {
        pagination: pagination,
        users: users.filter((item:User)=>item.id==props.search)
      };
    }
    else{
      return {
        pagination: pagination,
        users: users
      };
    }

	}
);



export const selectAllEntities = createSelector(selectUsersState, UserReducer.selectEntities);

export const entityExists = createSelector(selectAllEntities, (entities: any, props: any): boolean => {
	return entities[props.id] == undefined ? false : true;
});

export const selectEntityById = createSelector(
	selectAllEntities,
	(entities: any, props: any): any => entities[props.id]
);
