import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { User } from './user.model';
import * as UserActions from './user.actions';
import { Pagination } from 'src/app/shared/models/pagination';
import { isNgTemplate } from '@angular/compiler';

export const usersFeatureKey = 'users';

export interface State extends EntityState<User> {
	// additional entities state properties
	error: any;
	pagination: Pagination;
	selectedUserId: number | null;
	user: User;
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>();

export const initialState: State = adapter.getInitialState({
	// additional entity state properties
	error: null,
	pagination: {
		total: 0,
		per_page: 0,
		total_pages: 0,
		page: 0
	},
	selectedUserId: null,
	user: null
});

export const reducer = createReducer(
	initialState,
	on(UserActions.loadUsersSuccess, (state, action) => {
		return adapter.setAll(action.paginatedResults.data, {
			...state,
			pagination: {
				total: action.paginatedResults.total,
				page: action.paginatedResults.page,
				total_pages: action.paginatedResults.total_pages,
				per_page: action.paginatedResults.per_page
			}
		});
	}),
	on(UserActions.loadUsersFailure, (state, action) => {
		return {
			...state,
			error: action.error
		};
	}),
	on(UserActions.searchUsersSuccess, (state, action) => {
		return {
			...state,
			user: action.paginatedResults.data
		};
	}),
	on(UserActions.loadUsersFailure, UserActions.searchUsersFailure, (state, action) => {
		return {
			...state,
			error: action.error
		};
	}),
	on(UserActions.addUser, (state, action) => adapter.addOne(action.user, state)),
	on(UserActions.upsertUser, (state, action) => adapter.upsertOne(action.user, state)),
	on(UserActions.addUsers, (state, action) => adapter.addMany(action.users, state)),
	on(UserActions.upsertUsers, (state, action) => adapter.upsertMany(action.users, state)),
	on(UserActions.updateUser, (state, action) => adapter.updateOne(action.user, state)),
	on(UserActions.updateUsers, (state, action) => adapter.updateMany(action.users, state)),
	on(UserActions.deleteUser, (state, action) => adapter.removeOne(action.id, state)),
	on(UserActions.deleteUsers, (state, action) => adapter.removeMany(action.ids, state)),
	on(UserActions.clearUsers, (state) => adapter.removeAll(state))
);

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();
