import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserService } from '../services/user.service';
import * as UserActions from './user.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as fromUserSelectors from '../state/user.selectors';
import { AppState } from 'src/app/store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class UserEffects {
	constructor(private actions$: Actions, private userService: UserService, private store: Store<AppState>) {}

	/************************************************/
	/*** LOAD Users Effects***/
	/************************************************/
	loadUsers$ = createEffect(() => {
		return this.actions$.pipe(
			ofType(UserActions.loadUsers),
			mergeMap((action) =>
				this.userService
					.getAllUsers(action.page)
					.pipe(
						map((data) => UserActions.loadUsersSuccess({ paginatedResults: data })),
						catchError((error) => of(UserActions.loadUsersFailure({ error })))
					)
			)
		);
	});
	/******/

	searchUsers$ = createEffect(() => {
		return this.actions$.pipe(
			ofType(UserActions.searchUsers),
			mergeMap((action) =>
				this.userService
					.getUser(action.search)
					.pipe(
						map((data) => UserActions.searchUsersSuccess({ paginatedResults: data })),
						catchError((error) => of(UserActions.loadUsersFailure({ error })))
					)
			)
		);
	});
}
