import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../state/user.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PaginatedResult } from 'src/app/shared/models/pagination';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	baseUrl: string = environment.baseUrl + 'users';

	constructor(private http: HttpClient) {}

	getAllUsers(page: string): Observable<PaginatedResult<User[]>> {
		return this.http.get<PaginatedResult<User[]>>(this.baseUrl + `?page=${page}`);
	}

	getUser(search: string): Observable<PaginatedResult<User[]>> {
		return this.http.get<PaginatedResult<User[]>>(this.baseUrl + `/${search}`);
	}
}
