import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AppState } from 'src/app/store';
import * as fromUserActions from '../state/user.actions';
import * as fromUserSelectors from '../state/user.selectors';
import { User } from '../state/user.model';

@Component({
	selector: 'app-user-search-form',
	templateUrl: './user-search-form.component.html',
	styleUrls: [ './user-search-form.component.scss' ]
})
export class UserSearchFormComponent implements OnInit {
	id = '';
	// users$: Observable<User[]>;
	constructor(private store: Store<AppState>) {}

	ngOnInit(): void {
		// this.users$ = this.store.pipe(select(fromUserSelectors.selectAllUsers))
	}

	onChange() {
		this.store.dispatch(fromUserActions.searchUsers({ search: this.id }));
	}
}
