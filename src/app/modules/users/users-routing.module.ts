import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserViewComponent } from './user-view/user-view.component';
import { UsersComponent } from './users.component';

const routes: Routes = [
  {path:'users',component:UsersComponent},
  {path:'user-view/:id',component:UserViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
