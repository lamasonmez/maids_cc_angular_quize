import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserViewComponent } from './user-view/user-view.component';
import { UserItemComponent } from './user-item/user-item.component';
import { UserSearchFormComponent } from './user-search-form/user-search-form.component';
import { StoreModule } from '@ngrx/store';
import * as fromUser from './state/user.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './state/user.effects';
import { PaginationModule } from 'src/app/shared/pagination/pagination.module';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [ UsersComponent, UserViewComponent, UserItemComponent, UserSearchFormComponent ],
	imports: [
		CommonModule,
		UsersRoutingModule,
		FormsModule,
		StoreModule.forFeature(fromUser.usersFeatureKey, fromUser.reducer),
		EffectsModule.forFeature([ UserEffects ]),
		PaginationModule
	],
	exports: [ UserSearchFormComponent ]
})
export class UsersModule {}
