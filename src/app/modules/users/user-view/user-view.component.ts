import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import {Location} from '@angular/common';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { AppState } from 'src/app/store';
import { User } from '../state/user.model';

import * as fromUserSElecters from '../state/user.selectors';
@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  user$:Observable<User> | null = null;
  userId:string | null=null;
  isUserInStore$:Observable<boolean> | null = null;

  constructor(
    private route:ActivatedRoute,
    private store :Store<AppState>,
    private location:Location
  ) { }

  ngOnInit(): void {
    this.userId = this.route.snapshot.paramMap.get('id');

   this.isUserInStore$ =  this.store.pipe(
      select(fromUserSElecters.entityExists,{id:this.userId})
    );

   this.user$ = this.isUserInStore$.pipe(
    mergeMap((isUserInStore)=>{
      if(!isUserInStore){
        console.log('Get User from API')
      }
      return this.store.pipe(
        select(fromUserSElecters.selectEntityById,{
          id:this.userId
        })
      )
    })
  );

  }

}
