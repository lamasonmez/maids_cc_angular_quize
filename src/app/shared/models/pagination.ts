export interface Pagination {
  total:Number | number;
  per_page: Number | number;
  total_pages: Number | number;
  page: Number | number;//current page
}


export interface PaginatedResult<T> {
  data:T;
  total:Number | number,
  per_page: Number | number;
  total_pages: Number | number;
  page: Number | number;
}
