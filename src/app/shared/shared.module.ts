import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { NgxSpinnerModule } from "ngx-spinner";

import { UsersModule } from '../modules/users/users.module';

@NgModule({
  declarations: [HeaderComponent ],
  imports: [
    CommonModule,
    UsersModule,
    NgxSpinnerModule,


  ],
  exports:[
    HeaderComponent
  ]
})
export class SharedModule { }
