import { Component, Input, OnInit, Output, SimpleChange ,EventEmitter} from '@angular/core';
import paginate from '../helpers/paginate-helper';
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() totalItems: number=0;
  @Output() changePage = new EventEmitter<any>(true);
  @Input() initialPage = 1;
  @Input() pageSize = 10;
  @Input() maxPages = 10;
  pager: any = {};
  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChange) {
    // reset page if items array has changed
      this.setPage(this.initialPage);
}
setPage(page: number) {
  // get new pager object for specified page
  this.pager = paginate(this.totalItems, page, this.pageSize, this.maxPages);

  // call change page function in parent component
  this.changePage.emit(page);
}

}
